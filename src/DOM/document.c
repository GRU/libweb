#include <LibWeb/DOM/document.h>
#include <LibWeb/DOM/element.h>
#include <LibWeb/HTML/token.h>
#include <ctype.h>
#include <string.h>
#include <stddef.h>

Element create_element(Document document, DOMString local_name) { // FIXME: options
  
  Element element;

  // TODO: If localName does not match the Name production, then throw an "InvalidCharacterError" DOMException.

  // TODO: If this is an HTML document, then set localName to localName in ASCII lowercase.
  if (document.content_type == NULL || strcmp(document.content_type, "xml")) {
    for (size_t i = 0; i < strlen(local_name); i++)
      local_name[i] = tolower(local_name[i]);

    element.local_name = local_name;
  }

  // TODO: Let is be null.
  // TODO: If options is a dictionary and options["is"] exists, then set is to it.
  // TODO: Let namespace be the HTML namespace, if this is an HTML document or this’s content type is "application/xhtml+xml"; otherwise null.

  // TODO: Return the result of creating an element given this, localName, namespace, null, is, and with the synchronous custom elements flag set.

  return element;
}
