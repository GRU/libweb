#ifndef LIBWEB_DOM_CHARACTER_DATA_H
#define LIBWEB_DOM_CHARACTER_DATA_H

#include <LibWeb/DOM/types.h>

typedef struct CharacterData {
  DOMString data;
  unsigned long length;
} CharacterData;

// TODO: implement functions:

DOMString substring_data(CharacterData character_data, unsigned long offset, unsigned long count);
CharacterData append_data(CharacterData character_data, DOMString data);
CharacterData insert_data(CharacterData character_data, unsigned long offset, DOMString data);
CharacterData delete_data(CharacterData character_data, unsigned long offset, unsigned long count);
CharacterData replace_data(CharacterData character_data, unsigned long offset, unsigned long count, DOMString data);

#endif
