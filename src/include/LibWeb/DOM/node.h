#ifndef LIBWEB_DOM_NODE_H
#define LIBWEB_DOM_NODE_H

#include <LibWeb/DOM/document.h>
#include <LibWeb/DOM/text.h>
#include <LibWeb/DOM/types.h>
#include <LibWeb/HTML/token.h>
#include <stdbool.h>

typedef enum {
	ELEMENT_NODE = 1,
	ATTRIBUTE_NODE = 2,
	TEXT_NODE = 3,
	CDATA_SECTION_NODE = 4,
	ENTITY_REFERENCE_NODE = 5, // legacy
	ENTITY_NODE = 6, // legacy
	PROCESSING_INSTRUCTION_NODE = 7,
	COMMENT_NODE = 8,
	DOCUMENT_NODE = 9,
	DOCUMENT_TYPE_NODE = 10,
	DOCUMENT_FRAGMENT_NODE = 11,
	NOTATION_NODE = 12, // legacy
} node_type_t;

typedef enum {
  DOCUMENT_POSITION_DISCONNECTED = 0x01,
  DOCUMENT_POSITION_PRECEDING = 0x02,
  DOCUMENT_POSITION_FOLLOWING = 0x04,
  DOCUMENT_POSITION_CONTAINS = 0x08,
  DOCUMENT_POSITION_CONTAINED_BY = 0x10,
  DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 0x20,
} document_position_t;

typedef struct NodeList NodeList;

typedef struct Node {
  node_type_t node_type;
  DOMString node_name;
  // TODO: readonly attribute USVString baseURI;
  bool is_connected;
  // TODO: readonly attribute Document? ownerDocument;
  Document owner_document;

  struct Node *parent_node;
  // readonly attribute Element? parentElement;

  // TODO: NodeList child_nodes;
  struct Node *first_child;
  struct Node *last_child;
  struct Node *previous_sibling;
  struct Node *next_sibling;
} Node;

Node *init_document_node();
Node* make_node(node_type_t node_type, token_t token);
Node* make_text_node(Text text);

// TODO: Node getRootNode(optional GetRootNodeOptions options = {});
// TODO: boolean hasChildNodes();

// TODO: [CEReactions] attribute DOMString? nodeValue;
// TODO: [CEReactions] attribute DOMString? textContent;
// TODO: [CEReactions] undefined normalize();

// TODO: [CEReactions, NewObject] Node cloneNode(optional boolean deep = false);
// TODO: boolean isEqualNode(Node? otherNode);
// TODO: boolean isSameNode(Node? otherNode); // legacy alias of ===

// TODO: unsigned short compareDocumentPosition(Node other);
// TODO: boolean contains(Node? other);

// TODO: DOMString? lookupPrefix(DOMString? namespace);
// TODO: DOMString? lookupNamespaceURI(DOMString? prefix);
// TODO: boolean isDefaultNamespace(DOMString? namespace);

// TODO: [CEReactions] Node insertBefore(Node node, Node? child);

// TODO: [CEReactions] Node appendChild(Node node);
Node *append_child(Node *parent, Node *node);

// TODO: [CEReactions] Node replaceChild(Node node, Node child);
// TODO: [CEReactions] Node removeChild(Node child);

bool is_element_node(Node *node);
bool is_attribute_node(Node *node);
bool is_text_node(Node *node);
bool is_cdata_section_node(Node *node);
bool is_processing_instruction_node(Node *node);
bool is_comment_node(Node *node);
bool is_document_node(Node *node);
bool is_document_type_node(Node *node);
bool is_document_fragment_node(Node *node);

#endif
