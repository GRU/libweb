#ifndef LIBWEB_HTML_TOKEN_H
#define LIBWEB_HTML_TOKEN_H

#include <stdbool.h>

typedef struct {
  char *name;
  char *public_identifier;
  char *system_identifier;
  bool force_quirks;
} doctype_t;

typedef struct {
  char *name;
  char *value;
} attribute_t;

typedef struct {
  char *name;
  bool self_closing;
  attribute_t *attributes;
} tag_t;

typedef struct {
  char *data;
} comment_t;

typedef struct {
  char data;
} character_t;

typedef enum {
  UNKNOWN,
  DOCTYPE,
  START_TAG,
  END_TAG,
  COMMENT,
  CHARACTER,
  END_OF_FILE,
} token_type_t;

typedef struct {
  token_type_t type;
  doctype_t doctype;
  tag_t tag;
  comment_t comment;
  character_t character;
} token_t; // FIXME

bool is_doctype(token_t token);
bool is_start_tag(token_t token);
bool is_end_tag(token_t token);
bool is_comment(token_t token);
bool is_character(token_t token);
bool is_eof(token_t token);

char *doctype_name(token_t token);
char *tag_name(token_t token);
char *comment_data(token_t token);
char character_data(token_t token);

int dump_token(token_t token);

#endif
