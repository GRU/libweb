#include <LibWeb/HTML/stack.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

stack_t stack = NULL;
int top = 0;
int bottom = 0;

Node* last_node_on_stack() {
  if (bottom-1 < 0) {
    printf("WARNING: stack under-flow\n"); // FIXME
    return stack[bottom];
  }

  return stack[bottom-1];
}

Node* pop() {
  Node* element = stack[bottom];
  bottom--;
  return element;
}

void push(Node* element) {
  stack_t tmp = realloc(stack, (bottom+1)+sizeof(Node));
  tmp[bottom] = element;
  stack = tmp;
  bottom++;
}
