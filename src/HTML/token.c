#include <LibWeb/HTML/token.h>
#include <stdio.h>

bool is_doctype(token_t token) {
  return token.type == DOCTYPE;
}

bool is_start_tag(token_t token) {
  return token.type == START_TAG;
}

bool is_end_tag(token_t token) {
  return token.type == END_TAG;
}

bool is_comment(token_t token) {
  return token.type == COMMENT;
}

bool is_character(token_t token) {
  return token.type == CHARACTER;
}

bool is_eof(token_t token) {
  return token.type == END_OF_FILE;
}

char *doctype_name(token_t token) {
  return token.doctype.name;
}

char *tag_name(token_t token) {
  return token.tag.name;
}

char *comment_data(token_t token) {
  return token.comment.data;
}

char character_data(token_t token) {
  return token.character.data;
}

int dump_token(token_t token) {
  if (token.type == DOCTYPE) {
    printf("DOCTYPE token: %s\n", token.doctype.name);
  } else if (token.type == START_TAG || token.type == END_TAG) {
    if (token.tag.self_closing == false)
      printf("START_TAG");
    else
      printf("END_TAG");
    printf(" token: %s\n", token.tag.name);
  } else if (token.type == COMMENT) {
    printf("COMMENT token: %s\n", token.comment.data);
  } else if (token.type == CHARACTER) {
    printf("CHARACTER token: %c\n", token.character.data);
  } else if (token.type == END_OF_FILE) {
    printf("EOF token\n");
    return -1;
  } else {
    printf("Error: unknown token type");
    return -1;
  }
  return 0;
}
